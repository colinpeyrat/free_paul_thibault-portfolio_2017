import axios from 'axios';
import { ApiConfig } from '../util/Config';

export function fetchEmail() {
  return axios.get(ApiConfig.url + 'options/v1/email');
}

export function fetchAboutContent() {
  return axios.get(ApiConfig.url + 'wp/v2/pages/8');
}

export function fetchProjects() {
  return axios.get(ApiConfig.url + 'wp/v2/project');
}
