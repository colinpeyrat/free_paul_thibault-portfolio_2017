import { fetchEmail, fetchAboutContent, fetchProjects } from "../api";

export default {
  // get email adress
  FETCH_EMAIL: ({ commit }) => {
    return fetchEmail().then((email) => {
      commit("SET_EMAIL", email);
    });
  },

  // get about page content
  FETCH_ABOUT_CONTENT: ({ commit }) => {
    return fetchAboutContent().then(({ data }) => {
      commit("SET_ABOUT_CONTENT", data);
    });
  },

  // get projects
  FETCH_PROJECTS: ({ commit }) => {
    return fetchProjects().then(({ data }) => {
      commit("SET_PROJECTS", data);
    });
  },
};
