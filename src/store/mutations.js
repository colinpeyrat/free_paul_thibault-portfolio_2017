import Vue from 'vue';

export default {
  SET_EMAIL: (state, { data }) => {
    Vue.set(state, 'email', data);
  },

  SET_ABOUT_CONTENT: (state, { content }) => {
    Vue.set(state.content, 'about', content.rendered);
  },

  SET_PROJECTS: (state, projects) => {
    projects.forEach((project, key) => {
      Vue.set(state.projects, key, project);
    });
  }
};
